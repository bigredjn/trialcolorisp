import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import DocumentTitle from 'react-document-title';

import '../styles/App.css';

import LandingPage from './pages/landingPage';
import RequestForm from './pages/requestForm';
import ReadOurProject from './pages/readOurProject';
import ContactInfo from './pages/contactInfo';
import Submitted from './pages/submitted';

import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
    render() {
        return (
            <DocumentTitle title='Colorisp'>
                <Router>
                    <Switch>
                        <Route exact path="/" component={LandingPage} />
                        <ReadOurProject path="/project" component={ReadOurProject} />
                        <RequestForm path="/request" component={RequestForm}/>
                        <ContactInfo path="/contact" component={ContactInfo}/>
                        <Submitted path="/submitted" component={Submitted}/>
                    </Switch>
                </Router>
            </DocumentTitle>
        );
    }
}

export default App;
