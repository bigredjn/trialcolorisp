import React, { Component } from 'react';
// import { Link } from 'react-router-dom';


class Navigator extends Component {
  render() {
    return (
        <div>
            <h2>The Navigator</h2>
            <p>We give our subscribers a monthly review of the essentials that are happening in our office and the community. If you are interested, please use the form to sign up on our CONTACT US page.</p>
        </div>
      
    )
  }
}

export default Navigator;