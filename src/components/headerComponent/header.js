import React, { Component } from 'react';
import mainLogo from './ccdhhdb.png';
import "./header.css";

class Header extends Component {
  render() {
    return (
      <header>
        <div className="headerSection">
            <img src={mainLogo} className="logo" alt="Colorado Commission for the Deaf, Hard of Hearing, and Deafblind logo"></img>
            <div>
              <h2 className="headerTitle">RISP</h2>
              <p className="headerSubtitle"><i>Rural Interpreting Services Project (Pilot)</i></p>
            </div>
        </div>
      </header>
    )
  }
}

export default Header;