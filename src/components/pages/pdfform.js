import React, { Component } from 'react';
import { Button } from 'reactstrap';

import RISPform from '../pdf/RISPform.pdf'

class Download extends Component {

  render() {

    return (
      <div>
        <a href = {RISPform}><Button>Download Form</Button></a>
      </div>
    );
  }
}

export default Download;