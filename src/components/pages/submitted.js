import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';

import Header from '../headerComponent/header';
import Footer from '../footerComponent';
import "./submitted.css";

class Submitted extends Component {
  render() {
    return (
        <ul>
          <Header />
              <Link to="/"><Button>Home</Button></Link>
            <h2 className="Submitted">Thank you for submitting your request!</h2>
            <p className="Submitted">Please check your email for further information.  Be sure to check your spam folder. Please also ensure your email system allows emails from email_ccdhh@state.co.us</p>
          <Footer />
        </ul>
    )
  }
}

export default Submitted;