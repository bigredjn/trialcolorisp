import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom';
import { Button } from 'reactstrap';
import "./readOurProject.css";

import Header from '../headerComponent/header';
import Footer from '../footerComponent';

class ReadOurProject extends Component {
  render () {
    return (
    <div>
    <Header />
        <Link className="Button" to="/"><Button>Home</Button></Link>
    <Fragment>
      <h4 className="ReadOurProject">
        The Colorado Joint Budget Committee (JBC) has set aside $1.4 million over two years to address the following goals:
      </h4>
      <ul className="ReadOurProjectBody">
        <li>Goal #1: Provide ASL/English interpreting services in rural areas</li>
        <li>Goal #2: Provide grants for initial and advanced ASL/English interpreter training</li>
        <li>Goal #3: Outreach out to those who need service and those who may be able to provide such service</li>
      </ul>
      <Link to="/" className="Link">Click here to see Frequently Asked Questions (FAQs) about RISP!</Link>

    </Fragment>
    <Footer />
    </div>
    )
  }
}

export default ReadOurProject;