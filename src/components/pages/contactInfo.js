import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import "./contactInfo.css";

import Header from '../headerComponent/header';
import Footer from '../footerComponent';


class ContactInfo extends Component {
  render() {
    return (
        <div>
          <div>
            <Header />
          </div>
          <div>
            <Link to="/" alt="Website Homepage"><Button>Home</Button></Link>
          </div>
            <br></br>
            <div className="ContactInfo">
              <h1 className="ContactHeader">Colorado Department of Human Services</h1>
              <br></br>
              <h4>1575 Sherman Street Denver, CO 80203</h4>
              <br></br>
              <p><i>Phone: </i><b>720-457-3679</b></p>
              <p><i>Email: <a href = "mailto: email.ccdhh@state.co.us">email.ccdhh@state.co.us</a></i></p>
            </div>
          <div>
            <Footer />
          </div>
        </div>
    )
  }
}

export default ContactInfo;