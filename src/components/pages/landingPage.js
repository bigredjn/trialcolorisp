import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { Button } from 'reactstrap';
import "./landingPage.css";

import Header from '../headerComponent/header';
import Footer from '../footerComponent';

class LandingPage extends Component {
  render() {
    return (
      <div>
          <Header />
          <div>
          <div className="readSection">
            <p className="Subtitle">Providing free ASL/English interpreting services for rural Coloradans</p>
              <div>
                  <Link to="/project" alt="Read Our Project Homepage" text-align="center"><Button>About Our Project</Button></Link>
              </div>
          </div>
        </div>
        <div>
          <div className="reqSection">
            <p className="Subtitle">Schedule your request!</p>
              <div>
                  <Link to="/request" alt="Schedule Your Request Homepage"><Button>Request An Interpreter</Button></Link>
              </div>
          </div>
        </div>
          <div className="contactSection">
            <p className="Subtitle">Accomplish more by talking to us!</p>
              <div>
                  <Link to="/contact" alt="Contact Us Homepage"><Button>Connect With Us Today</Button></Link>
              </div>
          </div>
        <div>
        </div>
          <div className="InfomationTerp">
            <p className="Subtitle">Information For Interpreters</p>
              <div>
                  <Link to="/contact" alt="Contact Us Homepage"><Button>Connect With Us Today</Button></Link>
              </div>
          </div>
        <div>
          <Footer />
        </div>
      </div>
    )
  }
}

export default LandingPage;