import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom';
import { oneOfType, string, number } from 'prop-types';
import { Button, Input, Label } from 'reactstrap';

import Header from '../headerComponent/header';
import Footer from '../footerComponent';
import Download from './pdfform.js';
import "./requestForm.css";

const stringOrNumber = oneOfType([string, number]);

// local links
//const url = "http://localhost:3000"
const server = "http://localhost:4000"

// production links
// const url = "https://trialcolorisp.herokuapp.com"
// const server = "http://ec2-34-229-242-227.compute-1.amazonaws.com:4000"

class RequestForm extends Component {
    render() {
    return (
      <div>
        <Header />
        <RequestItem />
        <Footer />
      </div>
    )
  }
}

class RequestItem extends Component {
  static propTypes = {
    companyMaxLength: stringOrNumber,
    companyPlaceholder: string,
    firstNameRows: stringOrNumber,
    firstNameMaxLength: stringOrNumber,
    firstNamePlaceholder: string,
    lastNameRows: stringOrNumber,
    lastNameMaxLength: stringOrNumber,
    lastNamePlaceholder: string,
    addressRows: stringOrNumber,
    addressMaxLength: stringOrNumber,
    addressPlaceholder: string,
    cityRows: stringOrNumber,
    cityMaxLength: stringOrNumber,
    cityPlaceholder: string,
    zipCodeRows: stringOrNumber,
    zipCodeMaxLength: stringOrNumber,
    zipCodePlaceholder: string,
    phoneRows: stringOrNumber,
    phoneMaxLength: stringOrNumber,
    phonePlaceholder: string,
    emailRows: stringOrNumber,
    emailMaxLength: stringOrNumber,
    emailPlaceholder: string,
    contactRows: stringOrNumber,
    contactMaxLength: stringOrNumber,
    contactPlaceholder: string,
    contactCompanyRows: stringOrNumber,
    contactCompanyMaxLength: stringOrNumber,
    contactCompanyPlaceholder: string,
    contactAddressRows: stringOrNumber,
    contactAddressMaxLength: stringOrNumber,
    contactAddressPlaceholder: string,
    contactCityRows: stringOrNumber,
    contactCityMaxLength: stringOrNumber,
    contactCityPlaceholder: string,
    contactZipCodeRows: stringOrNumber,
    contactZipCodeMaxLength: stringOrNumber,
    contactZipCodePlaceholder: string,
    contactPhoneRows: stringOrNumber,
    contactPhoneMaxLength: stringOrNumber,
    contactPhonePlaceholder: string,
    contactEmailRows: stringOrNumber,
    contactEmailMaxLength: stringOrNumber,
    contactEmailPlaceholder: string,
    purposeRows: stringOrNumber,
    purposeMaxLength: stringOrNumber,
    purposePlaceholder: string,
    dateRows: stringOrNumber,
    dateMaxLength: stringOrNumber,
    datePlaceholder: string,
    startEndTimesRows: stringOrNumber,
    startEndTimesMaxLength: stringOrNumber,
    startEndTimesPlaceholder: string,
    locationRows: stringOrNumber,
    locationMaxLength: stringOrNumber,
    locationPlaceholder: string,
    assignCityRows: stringOrNumber,
    assignCityMaxLength: stringOrNumber,
    assignCityPlaceholder: string,
    assignZipCodeRows: stringOrNumber,
    assignZipCodeMaxLength: stringOrNumber,
    assignZipCodePlaceholder: string,
    buildingRows: stringOrNumber,
    buildingMaxLength: stringOrNumber,
    buildingPlaceholder: string,
    ongoingRows: stringOrNumber,
    ongoingMaxLength: stringOrNumber,
    ongoingPlaceholder: string,
    hearingStatusRows: stringOrNumber,
    hearingStatusMaxLength: stringOrNumber,
    hearingStatusPlaceholder: string,
    countRows: stringOrNumber,
    countMaxLength: stringOrNumber,
    countPlaceholder: string,
    individualNamesRows: stringOrNumber,
    individualNamesMaxLength: stringOrNumber,
    individualNamesPlaceholder: string,
    yesContactRows: stringOrNumber,
    yesContactMaxLength: stringOrNumber,
    yesContactPlaceholder: string,
    otherRows: stringOrNumber,
    otherMaxLength: stringOrNumber,
    otherPlaceholder: string,
    additionalRows: stringOrNumber,
    additionalMaxLength: stringOrNumber,
    additionalPlaceholder: string,
  };

  static defaultProps = {
    firstNameRows: 8,
    firstNameMaxLength: 500,
    firstNamePlaceholder: 'First Name',
    lastNameRows: 8,
    lastNameMaxLength: 500,
    lastNamePlaceholder: 'Last Name',
    companyMaxLength: 50,
    companyPlaceholder: 'Company Name',
    addressRows: 8,
    addressMaxLength: 500,
    addressPlaceholder: 'Address',
    cityRows: 8,
    cityMaxLength: 500,
    cityPlaceholder: 'City',
    phoneRows: 8,
    phoneMaxLength: 500,
    phonePlaceholder: 'Phone Number',
    emailRows: 8,
    emailMaxLength: 500,
    emailPlaceholder: 'Email Address',
    contactRows: 8,
    contactMaxLength: 500,
    contactPlaceholder: 'Contact Name',
    contactCompanyRows: 8,
    contactCompanyMaxLength: 500,
    contactCompanyPlaceholder: 'Contact Company',
    contactAddressRows: 8,
    contactAddressMaxLength: 500,
    contactAddressPlaceholder: 'Contact Address',
    contactCityRows: 8,
    contactCityMaxLength: 500,
    contactCityPlaceholder: 'Contact City',
    contactZipCodeRows: 8,
    contactZipCodeMaxLength: 500,
    contactZipCodePlaceholder: 'Contact Zip Code',
    contactPhoneRows: 8,
    contactPhoneMaxLength: 500,
    contactPhonePlaceholder: 'Contact Phone',
    contactEmailRows: 8,
    contactEmailMaxLength: 500,
    contactEmailPlaceholder: 'Contact Email',
    purposeRows: 8,
    purposeMaxLength: 500,
    purposePlaceholder: 'Purpose',
    dateRows: 8,
    dateMaxLength: 500,
    datePlaceholder: 'Date',
    startEndTimesRows: 8,
    startEndTimesMaxLength: 500,
    startEndTimesPlaceholder: 'Start End Times',
    locationRows: 8,
    locationMaxLength: 500,
    locationPlaceholder: 'Location',
    assignCityRows: 8,
    assignCityMaxLength: 500,
    assignCityPlaceholder: 'Assign City',
    assignZipCodeRows: 8,
    assignZipCodeMaxLength: 500,
    assignZipCodePlaceholder: 'Assign Zip Code',
    buildingRows: 8,
    buildingMaxLength: 500,
    buildingPlaceholder: 'Building',
    ongoingRows: 8,
    ongoingMaxLength: 500,
    ongoingPlaceholder: 'Ongoing',
    hearingStatusRows: 8,
    hearingStatusMaxLength: 500,
    hearingStatusPlaceholder: 'Hearing Status',
    countRows: 8,
    countMaxLength: 500,
    countPlaceholder: 'Provide Head Count',
    individualNamesRows: 8,
    individualNamesMaxLength: 500,
    individualNamesPlaceholder: 'Individual Names',
    yesContactRows: 8,
    yesContactMaxLength: 500,
    yesContactPlaceholder: 'Individual Contact Information',
    otherRows: 8,
    otherMaxLength: 500,
    otherPlaceholder: 'Other',
    additionalCodeRows: 8,
    additionalCodeMaxLength: 500,
    additionalCodePlaceholder: 'Additional',
  };

  state = {
    // recipient: '', //
    sender: 'email@colorisp.com', //
    name: 'RISP Interpreter Request', //
    firstName: '', //
    lastName: '', //
    company: '', //
    address: '', //
    phoneType: '',
    phone: '', //
    email: '', //
    contact: '', //
    contactAddress: '', //
    contactPhone: '', //
    contactEmail: '', //
    purpose: '', //
    date: '', //
    startEndTimes: '', //
    location: '', //
    building: '',
    ongoing: '',
    hearingStatus: '',
    count: '',
    verified: '',
    individualNames: '',
    notified: '',
    yesContact: '',
    assignment: '',
    other: '',
    additional: '',
  }
  
  changeRadio = (e) => {
    this.setState({phoneType: e});
  }

  changeRadioHS = (e) => {
    this.setState({hearingStatus: e});
  }

  changeRadioYN = (e) => {
    this.setState({notified: e});
  }

  changeRadioAss = (e) => {
    this.setState({assignment: e});
  }
  
  changeRadioVerified = (e) => {
    this.setState({verified: e});
  }

  handleFirstName = (e) => {
      this.setState({ firstName: e.target.value });
    };

  handleLastName = (e) => {
    this.setState({ lastName: e.target.value });
  };
    
  handleCompany = (e) => {
        this.setState({ company: e.target.value });
    };

  handleAddress = (e) => {
    this.setState({ address: e.target.value });
  };

  handleCity = (e) => {
    this.setState({ city: e.target.value });
  };

  handlePhone = (e) => {
    this.setState({ phone: e.target.value });
  };

  handleEmail = (e) => {
    this.setState({ email: e.target.value });
  };

  handleContact = (e) => {
    this.setState({ contact: e.target.value });
  };

  handleContactCompany = (e) => {
    this.setState({ contactCompany: e.target.value });
  };

  handleContactAddress = (e) => {
    this.setState({ contactAddress: e.target.value });
  };

  handleContactCity = (e) => {
    this.setState({ contactCity: e.target.value });
  };

  handleContactZipCode = (e) => {
    this.setState({ contactZipCode: e.target.value });
  };

  handleContactPhone = (e) => {
    this.setState({ contactPhone: e.target.value });
  };

  handleContactEmail = (e) => {
    this.setState({ contactEmail: e.target.value });
  };

  handlePurpose = (e) => {
    this.setState({ purpose: e.target.value });
  };

  handleDate = (e) => {
    this.setState({ date: e.target.value });
  };

  handleStartEndTimes = (e) => {
    this.setState({ startEndTimes: e.target.value });
  };

  handleLocation = (e) => {
    this.setState({ location: e.target.value });
  };

  handleAssignCity = (e) => {
    this.setState({ assignCity: e.target.value });
  };

  handleAssignZipCode = (e) => {
    this.setState({ assignZipCode: e.target.value });
  };

  handleBuilding = (e) => {
    this.setState({ building: e.target.value });
  };

  handleOngoing = (e) => {
    this.setState({ ongoing: e.target.value });
  };

  handleHearingStatus = (e) => {
    this.setState({ hearingStatus: e.target.value });
  };

  handleCount = (e) => {
    this.setState({ count: e.target.value });
  };

  handleIndividualNames = (e) => {
    this.setState({ individualNames: e.target.value });
  };

  handleYesContact = (e) => {
    this.setState({ yesContact: e.target.value });
  };

  handleOther = (e) => {
    this.setState({ other: e.target.value });
  };

  handleAdditional = (e) => {
    this.setState({ additional: e.target.value });
  };

  sendAndRedirect = async event => {
    const { recipient, sender, name, firstName, lastName, company, address, phoneType, phone, email, contact, contactAddress, contactPhone, contactEmail, purpose, date, startEndTimes, location, building, hearingStatus, count, verified, individualNames, notified, yesContact, assignment, other, additional } = this.state;
    if (this.state.firstName === "" || this.state.lastName === "" || this.state.company === "" || this.state.city === "" || this.state.email === "" || this.state.date === "" || this.state.startEndTimes === "" || this.state.location === "" || this.state.assignCity === "" || this.state.count === "" || this.state.verified === "" || this.state.individualNames === "") {
      event.preventDefault();
      return alert(`Please enter in all required fields that has a "*".`)
    }
      try {
        let response = await fetch(`${server}/send-email?recipient=${recipient}&sender=${sender}&topic=${name}&company=${company}&address=${address}&firstName=${firstName}&lastName=${lastName}&phoneType=${phoneType}&phone=${phone}&email=${email}&contact=${contact}&contactAddress=${contactAddress}&contactPhone=${contactPhone}&contactEmail=${contactEmail}&purpose=${purpose}&date=${date}&startEndTimes=${startEndTimes}&location=${location}&building=${building}&hearingStatus=${hearingStatus}&count=${count}&verified=${verified}&notified=${notified}&individualNames=${individualNames}&yesContact=${yesContact}&assignment=${assignment}&other=${other}&additional=${additional}`, { 
              method: 'GET',
              headers:{
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials':true,
                'Access-Control-Allow-Methods':'POST, GET'
              }
            })
        let json = await response.json();
        console.log(json);
      }
      catch(e) {
        console.log('Error!', e);
      }
    }
  
  
    render() {
        const {
            firstNameRows,
            firstNameMaxLength,
            firstNamePlaceholder,
            lastNameRows,
            lastNameMaxLength,
            lastNamePlaceholder,
            companyMaxLength,
            companyPlaceholder,
            addressRows,
            addressMaxLength,
            addressPlaceholder,
            cityRows,
            cityMaxLength,
            cityPlaceholder,
            phoneRows,
            phoneMaxLength,
            phonePlaceholder,
            emailRows,
            emailMaxLength,
            emailPlaceholder,
            contactRows,
            contactMaxLength,
            contactPlaceholder,
            contactAddressRows,
            contactAddressMaxLength,
            contactAddressPlaceholder,
            contactPhoneRows,
            contactPhoneMaxLength,
            contactPhonePlaceholder,
            contactEmailRows,
            contactEmailMaxLength,
            contactEmailPlaceholder,
            purposeRows,
            purposeMaxLength,
            purposePlaceholder,
            dateRows,
            dateMaxLength,
            datePlaceholder,
            startEndTimesRows,
            startEndTimesMaxLength,
            startEndTimesPlaceholder,
            locationRows,
            locationMaxLength,
            locationPlaceholder,
            assignCityRows,
            assignCityMaxLength,
            assignCityPlaceholder,
            buildingRows,
            buildingMaxLength,
            buildingPlaceholder,
            countRows,
            countMaxLength,
            countPlaceholder,
            individualNamesRows,
            individualNamesMaxLength,
            individualNamesPlaceholder,
            yesContactRows,
            yesContactMaxLength,
            yesContactPlaceholder,
            otherRows,
            otherMaxLength,
            otherPlaceholder,
            additionalRows,
            additionalMaxLength,
            additionalPlaceholder,
          } = this.props;

    return (
      <Fragment>
          <Link to="/" alt="Website Homepage"><Button>Home</Button></Link>
          <br></br>
          <br></br>
          <Download alt="Request Form in PDF format"/>
          <br></br>
      <div className="body">
        <Label>Requester *</Label>
        <br></br>
        <br></br>
        <Label>First Name</Label>
        <Input
          aria-required="true"
          type="text" name="firstname"
          value={this.state.firstName}
          onChange={this.handleFirstName}
          rows={firstNameRows}
          maxLength={firstNameMaxLength}
          placeholder={firstNamePlaceholder}
        />
        <br></br>
        <Label>Last Name</Label>
        <Input
          aria-required="true"
          type="text" name="lastname"
          value={this.state.lastName}
          onChange={this.handleLastName}
          rows={lastNameRows}
          maxLength={lastNameMaxLength}
          placeholder={lastNamePlaceholder}
        />
        <br></br>
      <Label>Name of Company/ Business/ Other *</Label>
      <Input
          aria-required="true"
          type="text" name="Company"
          value={this.state.company}
          onChange={this.handleCompany}
          maxLength={companyMaxLength}
          placeholder={companyPlaceholder}
        />
        <br></br>
        <Label>Address</Label>
        <Input
          type="text" name="Address"
          value={this.state.address}
          onChange={this.handleAddress}
          rows={addressRows}
          maxLength={addressMaxLength}
          placeholder={addressPlaceholder}
        />
        <br></br>
        <Label>City/Town * </Label>
        <Input
          aria-required="true"
          type="text" name="City"
          value={this.state.city}
          onChange={this.handleCity}
          rows={cityRows}
          maxLength={cityMaxLength}
          placeholder={cityPlaceholder}
        />
        <br></br>
        <Label>Phone: </Label>
        <br></br>
          <input 
            type='radio'
            value='Home'
            name='phoneType'
            checked={this.state.phoneType === 'Home'}
            onChange={this.changeRadio.bind(this, 'Home')} />
            Home
            <br></br>
          <input 
            type='radio'
            value='Work' 
            name='phoneType'
            checked={this.state.phoneType === 'Work'}
            onChange={this.changeRadio.bind(this, 'Work')} />
            Work
            <br></br>
          <input 
            type='radio'
            value='Mobile' 
            name='phoneType'
            checked={this.state.phoneType === 'Mobile'}
            onChange={this.changeRadio.bind(this, 'Mobile')} />
            Mobile
            <br></br>
            <br></br>
        <Input
          type="phone" name="phone"
          value={this.state.phone}
          onChange={this.handlePhone}
          rows={phoneRows}
          maxLength={phoneMaxLength}
          placeholder={phonePlaceholder}
        />
        <br></br>
        <Label>Email Address *</Label>
        <Input
          aria-required="true"
          type="email" name="email"
          value={this.state.email}
          onChange={this.handleEmail}
          rows={emailRows}
          maxLength={emailMaxLength}
          placeholder={emailPlaceholder}
        />
        <br></br>
        <Label>On site: Name of contact person</Label>
        <Input
          type="contact" name="contact"
          value={this.state.contact}
          onChange={this.handleContact}
          rows={contactRows}
          maxLength={contactMaxLength}
          placeholder={contactPlaceholder}
        />
        <br></br>
        <Label>Contact Person Address</Label>
        <Input
          type="contactAddress" name="contactAddress"
          value={this.state.contactAddress}
          onChange={this.handleContactAddress}
          rows={contactAddressRows}
          maxLength={contactAddressMaxLength}
          placeholder={contactAddressPlaceholder}
        />
        <br></br>
        <Label>Contact Person Phone</Label>
        <Input
          type="contactPhone" name="contactPhone"
          value={this.state.contactPhone}
          onChange={this.handleContactPhone}
          rows={contactPhoneRows}
          maxLength={contactPhoneMaxLength}
          placeholder={contactPhonePlaceholder}
        />
        <br></br>
        <Label>Contact Person Email</Label>
        <Input
          type="contactEmail" name="contactEmail"
          value={this.state.contactEmail}
          onChange={this.handleContactEmail}
          rows={contactEmailRows}
          maxLength={contactEmailMaxLength}
          placeholder={contactEmailPlaceholder}
        />
        <br></br>
        <Label>Assignment Details: Purpose of request/assignment?</Label>
        <Input
          type="purpose" name="purpose"
          value={this.state.purpose}
          onChange={this.handlePurpose}
          rows={purposeRows}
          maxLength={purposeMaxLength}
          placeholder={purposePlaceholder}
        />
        <br></br>
        <Label>Date of Assignment *</Label>
        <Input
          aria-required="true"
          type="date" name="date"
          value={this.state.date}
          onChange={this.handleDate}
          rows={dateRows}
          maxLength={dateMaxLength}
          placeholder={datePlaceholder}
        />
        <br></br>
        <Label>Start/End Times *</Label>
        <Input
          aria-required="true"
          type="startEndTimes" name="startEndTimes"
          value={this.state.startEndTimes}
          onChange={this.handleStartEndTimes}
          rows={startEndTimesRows}
          maxLength={startEndTimesMaxLength}
          placeholder={startEndTimesPlaceholder}
        />
        <br></br>
        <Label>Location/Address of Assignment: *</Label>
        <Input
          aria-required="true"
          type="location" name="location"
          value={this.state.location}
          onChange={this.handleLocation}
          rows={locationRows}
          maxLength={locationMaxLength}
          placeholder={locationPlaceholder}
        />
        <br></br>
        <Label>City/Town *</Label>
        <Input
          aria-required="true"
          type="assignCity" name="assignCity"
          value={this.state.assignCity}
          onChange={this.handleAssignCity}
          rows={assignCityRows}
          maxLength={assignCityMaxLength}
          placeholder={assignCityPlaceholder}
        />
        <br></br>
        <Label>Building/room/floor information</Label>
        <Input
          type="building" name="building"
          value={this.state.building}
          onChange={this.handleBuilding}
          rows={buildingRows}
          maxLength={buildingMaxLength}
          placeholder={buildingPlaceholder}
        />
        <br></br>
        <label> If Known: </label>
          <br></br>
          <div>
          <input 
            type='radio'
            value='Deaf'
            name='hearingStatus'
            checked={this.state.hearingStatus === 'Deaf'}
            onChange={this.changeRadioHS.bind(this, 'Deaf')} />
            Deaf
            <br></br>
          <input 
            type='radio'
            value='hardOfHearing' 
            name='hearingStatus'
            checked={this.state.hearingStatus === 'hardOfHearing'}
            onChange={this.changeRadioHS.bind(this, 'hardOfHearing')} />
            Hard of Hearing
            <br></br>
          <input 
            type='radio'
            value='DeafBlind' 
            name='hearingStatus'
            checked={this.state.hearingStatus === 'DeafBlind'}
            onChange={this.changeRadioHS.bind(this, 'DeafBlind')} />
            DeafBlind
            <br></br>
          </div>
        <br></br>
        <Label>How many individuals will be using the interpreting services? *</Label>
        <Input
          aria-required="true"
          type="count" name="count"
          value={this.state.count}
          onChange={this.handleCount}
          rows={countRows}
          maxLength={countMaxLength}
          placeholder={countPlaceholder}
        />
        <br></br>
        <label> Verify: the individual(s) uses sign language? *</label>
        <br></br>
        <input 
            type='radio'
            value='Yes'
            name='verified'
            checked={this.state.verified === 'Yes'}
            onChange={this.changeRadioVerified.bind(this, 'Yes')} />
            Yes
            <br></br>
          <input 
            type='radio'
            value='No' 
            name='verified'
            checked={this.state.verified === 'No'}
            onChange={this.changeRadioVerified.bind(this, 'No')} />
            No
        <br></br>
        <Label>Individual Names *</Label>
        <Input
          aria-required="true"
          type="individualNames" name="individualNames"
          value={this.state.individualNames}
          onChange={this.handleIndividualNames}
          rows={individualNamesRows}
          maxLength={individualNamesMaxLength}
          placeholder={individualNamesPlaceholder}
        />
        <br></br>
        <label> Does the individual(s) want to be notified when an interpreter(s) has been scheduled? </label>
        <br></br>
        <input 
            type='radio'
            value='Yes'
            name='notified'
            checked={this.state.notified === 'Yes'}
            onChange={this.changeRadioYN.bind(this, 'Yes')} />
            Yes
            <br></br>
          <input 
            type='radio'
            value='No' 
            name='notified'
            checked={this.state.notified === 'No'}
            onChange={this.changeRadioYN.bind(this, 'No')} />
            No
          <br></br>
          <br></br>
        <Label>If Yes, Please Provide Contact Information</Label>
        <Input
          type="yesContact" name="yesContact"
          value={this.state.yesContact}
          onChange={this.handleYesContact}
          rows={yesContactRows}
          maxLength={yesContactMaxLength}
          placeholder={yesContactPlaceholder}
        />
        <br></br>
        <label> Mark type of situation or assignment; </label>
          <br></br>
          <input 
            type='radio'
            value='Emergency'
            name='assignment'
            checked={this.state.assignment === 'Emergency'}
            onChange={this.changeRadioAss.bind(this, 'Emergency')} />
            Emergency
            <br></br>
          <input 
            type='radio'
            value='Surgery' 
            name='assignment'
            checked={this.state.assignment === 'Surgery'}
            onChange={this.changeRadioAss.bind(this, 'Surgery')} />
            Surgery
            <br></br>
            <input 
            type='radio'
            value='Medical'
            name='assignment'
            checked={this.state.assignment === 'Medical'}
            onChange={this.changeRadioAss.bind(this, 'Medical')} />
            Medical
            <br></br>
          <input 
            type='radio'
            value='Dentist' 
            name='assignment'
            checked={this.state.assignment === 'Dentist'}
            onChange={this.changeRadioAss.bind(this, 'Dentist')} />
            Dentist
            <br></br>
            <input 
            type='radio'
            value='Mental health'
            name='assignment'
            checked={this.state.assignment === 'Mental health'}
            onChange={this.changeRadioAss.bind(this, 'Mental health')} />
            Mental Health
            <br></br>
          <input 
            type='radio'
            value='Group meeting' 
            name='assignment'
            checked={this.state.assignment === 'Group meeting'}
            onChange={this.changeRadioAss.bind(this, 'Group meeting')} />
            Group Meeting
            <br></br>
            <input 
            type='radio'
            value='Ono-on-ono'
            name='assignment'
            checked={this.state.assignment === 'Ono-on-ono'}
            onChange={this.changeRadioAss.bind(this, 'Ono-on-ono')} />
            Ono-on-One Meeting
            <br></br>
          <input 
            type='radio'
            value='Legal: municipal' 
            name='assignment'
            checked={this.state.assignment === 'Legal: municipal'}
            onChange={this.changeRadioAss.bind(this, 'Legal: municipal')} />
            Legal: Municipal Court
            <br></br>
            <input 
            type='radio'
            value='Legal Meeting'
            name='assignment'
            checked={this.state.assignment === 'Legal Meeting'}
            onChange={this.changeRadioAss.bind(this, 'Legal Meeting')} />
            Legal: Meeting with Attorney/ Legal advisor
            <br></br>
          <input 
            type='radio'
            value='Law Enforcement' 
            name='assignment'
            checked={this.state.assignment === 'Law Enforcement'}
            onChange={this.changeRadioAss.bind(this, 'Law Enforcement')} />
            Law Enforcement: Police, Sheriff, State Patrol
            <br></br>
          <input 
            type='radio'
            value='Employment-Related' 
            name='assignment'
            checked={this.state.assignment === 'Employment-Related'}
            onChange={this.changeRadioAss.bind(this, 'Employment-Related')} />
            Employment-Related
            <br></br>
            <input 
            type='radio'
            value='Event'
            name='assignment'
            checked={this.state.assignment === 'Event'}
            onChange={this.changeRadioAss.bind(this, 'Event')} />
            Event
            <br></br>
          <input 
            type='radio'
            value='Presentation' 
            name='assignment'
            checked={this.state.assignment === 'Presentation'}
            onChange={this.changeRadioAss.bind(this, 'Presentation')} />
            Presentation
            <br></br>
            <input 
            type='radio'
            value='Training'
            name='assignment'
            checked={this.state.assignment === 'Training'}
            onChange={this.changeRadioAss.bind(this, 'Training')} />
            Training
            <br></br>
          <input 
            type='radio'
            value='Other Govt-related' 
            name='assignment'
            checked={this.state.assignment === 'Other Govt-related'}
            onChange={this.changeRadioAss.bind(this, 'Other Govt-related')} />
            Other Government-Related
            <br></br>
            <input 
            type='radio'
            value='Other'
            name='assignment'
            checked={this.state.assignment === 'Other'}
            onChange={this.changeRadioAss.bind(this, 'Other')} />
            Other
          <br></br>
        <Label>If other, please specify</Label>
        <Input
          type="other" name="other"
          value={this.state.other}
          onChange={this.handleOther}
          rows={otherRows}
          maxLength={otherMaxLength}
          placeholder={otherPlaceholder}
        />
        <br></br>
        <Label>Any comment?</Label>
        <Input
          type="additional" name="additional"
          value={this.state.additional}
          onChange={this.handleAdditional}
          rows={additionalRows}
          maxLength={additionalMaxLength}
          placeholder={additionalPlaceholder}
        />
        <br></br>
        <Link to="/submitted" alt="Submit to Send Request Form"><Button onClick={this.sendAndRedirect} > Send Email </Button></Link>
        <br></br>
        <br></br>
       </div> 
    </Fragment>
    )
  }
}

export default RequestForm;