const express = require('express'); //needed to launch server
const cors = require('cors'); //needed to disable sendgrid security
const port = process.env.PORT || 4000;
const sgMail = require('@sendgrid/mail'); //sendgrid library to send emails 

const app = express(); //alias from the express function
// const port = process.env.PORT || 3001;

const corsOptions = {
    origin: "*",
    methods: "GET, HEAD, PUT, PATCH, POST, DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
  };

require('dotenv').config()
//sendgrid api key
sgMail.setApiKey(process.env.SENDGRID_API_KEY); 

app.use(cors()); //utilize Cors so the browser doesn't restrict data, without it Sendgrid will not send!

// Welcome page of the express server: 
app.get('/', (req, res) => {
    res.send("Welcome to the Sendgrid Emailing Server"); 
});

app.get('/send-email', (req,res) => {
    
    //Get Variables from query string in the search bar
    const { recipient, sender, topic, name, firstName, lastName, company, address, phoneType, phone, email, contact, contactAddress, contactPhone, contactEmail, purpose, date, startEndTimes, location, building, hearingStatus, count, verified, individualNames, notified, yesContact, assignment, other, additional } = req.query; 
    const emails = [
        {
        // Confirmation email
        to: ['jason.a.nuhn@gmail.com'], 
        from: sender,
        subject: topic,
        html: `Requestor Contact Information:`+`<br>`+
        `From: `+firstName+' '+lastName+`<br>`+
        `Business: `+company+`<br>`+
        `Address: `+address+`<br>`+
        `Type: `+phoneType+`<br>`+
        `Digits: `+phone+`<br>`+
        `Email: `+email+`<br>`+
        `<br>`+
        `On-Site Contact:`+`<br>`+
        `Name: `+contact+`<br>`+
        // `Business:`+contactCompany+`<br>`+
        `Address: `+contactAddress+`<br>`+
        // `Type:Home`+`<br>`+
        `Digits: `+contactPhone+`<br>`+
        `Email: `+contactEmail+`<br>`+
        `<br>`+
        `Assignment Details:`+`<br>`+
        `Purpose of request/assignment: `+purpose+`<br>`+
        `Date: `+date+`<br>`+
        `Start/End Time: `+startEndTimes+`<br>`+
        `Address: `+location+`<br>`+
        `Building/floor/room: `+building+`<br>`+
        // `One time or ongoing:One time`+`<br>`+
        // `If ongoing, specify:`+`<br>`+
        `If known: `+hearingStatus+`<br>`+
        `How many individuals: `+count+`<br>`+
        `Verify: `+verified+`<br>`+
        `Individual(s) Name: `+individualNames+`<br>`+
        `Notification: `+notified+`<br>`+
        `Contact information: `+yesContact+`<br>`+
        `<br>`+
        `Type of Assignment:`+assignment+`<br>`+
        // `Preparatory Materials:No`+`<br>`+
        // `Type of Assignment:Other`+other+`<br>`+
        `If other please specify: `+other+`<br>`+
        `Additional information: `+additional+`<br>`,
        // text: `This e-mail was sent from a contact form on Colorado Commission for the Deaf & Hard of Hearing (http://ccdhh.com)`,
    },
    {
        // auto-reply email
        to: email, 
        from: sender,
        subject: topic,
        html: `Your interpreting request for `+assignment+` on `+date+` for `+startEndTimes+` has been received. Please give RISP Staff up to 48 business hours to respond to your request. If you have any questions and/or for some reason, you do not hear from us in that time frame, please contact us at ccdhhdb_risp@state.co.us to follow up. Thank you!`
    },
    ];

    // Send Email
    sgMail.send(emails)
    .then((emails) => {
        console.log(email);
    }).catch((error) => {
        //Log friendly error
        console.error(error.toString());

        //Extract error msg
        const {message, code, response} = error;

        //Extract response msg
        const {headers, body} = response;
        });
});

// to access server run 'nodemon index.js' then click here: http://localhost:4000/
// app.listen(3000, () => console.log("Running on Port 3000"));
app.listen(port, () => {
    console.log(`Server up and running on ${port}`);
  });